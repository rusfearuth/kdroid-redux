package su.rusfearuth.arch.kdroidredux.core.reducer

import su.rusfearuth.arch.kdroidredux.core.interfaces.State


data class TestState (val testMessage: String) : State {

  override fun makeCopy(): TestState = copy()
}