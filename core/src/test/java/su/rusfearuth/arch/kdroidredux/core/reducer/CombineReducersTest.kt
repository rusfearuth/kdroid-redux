package su.rusfearuth.arch.kdroidredux.core.reducer


import org.junit.Assert.*
import org.junit.Test
import su.rusfearuth.arch.kdroidredux.core.actions.InitAction
import su.rusfearuth.arch.kdroidredux.core.actions.ReplaceAction

class CombineReducersTest {

  @Test
  fun `should be created combined reducer`() {

    val reducer = combineReducers(reducerA, reducerB)

    val stateAfterInitAction = reducer(TestState("test"), InitAction)
    val state = reducer(stateAfterInitAction, ReplaceAction)

    assertEquals("Message should be `test A B`",
                 "test A B",
                 state.testMessage)
  }

}