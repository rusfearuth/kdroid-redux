package su.rusfearuth.arch.kdroidredux.core.reducer

import su.rusfearuth.arch.kdroidredux.core.actions.InitAction
import su.rusfearuth.arch.kdroidredux.core.actions.ReplaceAction

val reducerA: Reducer<TestState> = { state, action ->
  when (action) {
    is InitAction -> state.copy(testMessage = "${state.testMessage} A")
    else -> state
  }
}

val reducerB: Reducer<TestState> = { state, action ->
  when(action) {
    is ReplaceAction ->state.copy(testMessage = "${state.testMessage} B")
    else -> state
  }
}
