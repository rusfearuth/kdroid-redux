package su.rusfearuth.arch.kdroidredux.core.reducer

import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.interfaces.State


typealias Reducer<T> = (T, Action) -> T


fun <T: State> combineReducers(vararg reducers: Reducer<T>): Reducer<T> {
  return fun (state: T, action: Action): T {
    return reducers.fold(state) { acc, item -> item(acc, action) }
  }
}
