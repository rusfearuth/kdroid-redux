package su.rusfearuth.arch.kdroidredux.core.subscription


typealias Subscriber<T> = (T) -> Unit
