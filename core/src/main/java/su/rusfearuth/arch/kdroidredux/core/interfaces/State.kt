package su.rusfearuth.arch.kdroidredux.core.interfaces


interface State {
  fun makeCopy(): State
}
