package su.rusfearuth.arch.kdroidredux.core.middleware

import su.rusfearuth.arch.kdroidredux.core.dispatcher.Dispatcher
import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.interfaces.CommonStore


/**
 *
 */
typealias Middleware<T> = (CommonStore<T>) -> (Dispatcher) -> (Action) -> Action


/**
 * This type alias describes Enhancer signature
 *
 * The main idea of Enhancer to create some wrapper around [Store] and
 * modify dispatch method of original [Store].
 */
typealias Enhancer<T> = (T) -> T
