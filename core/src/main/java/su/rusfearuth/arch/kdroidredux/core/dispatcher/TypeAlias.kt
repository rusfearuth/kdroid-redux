package su.rusfearuth.arch.kdroidredux.core.dispatcher

import su.rusfearuth.arch.kdroidredux.core.interfaces.Action


typealias Dispatcher = (Action) -> Action
