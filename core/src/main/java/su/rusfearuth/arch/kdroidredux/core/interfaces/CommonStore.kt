package su.rusfearuth.arch.kdroidredux.core.interfaces

import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer


interface CommonStore<T: State> {

  val state: T

  val isDispatching: Boolean

  fun dispatch(action: Action): Action

  fun replaceReducer(reducer: Reducer<T>)

}
