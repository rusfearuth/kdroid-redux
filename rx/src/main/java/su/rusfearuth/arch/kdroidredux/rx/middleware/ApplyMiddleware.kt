package su.rusfearuth.arch.kdroidredux.rx.middleware

import io.reactivex.disposables.Disposable
import su.rusfearuth.arch.kdroidredux.core.dispatcher.Dispatcher
import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.middleware.Enhancer
import su.rusfearuth.arch.kdroidredux.core.middleware.Middleware
import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber
import su.rusfearuth.arch.kdroidredux.rx.store.Store


fun <T: State> applyMiddleware(vararg middlewares: Middleware<T>): Enhancer<Store<T>> {
  return fun(store: Store<T>): Store<T> {
    return object : Store<T> {

      private var dispatcher: Dispatcher = {
        throw Throwable("Dispatching while constructing your middleware is not allowed.\n" +
                            "Other middleware would not be applied to this dispatch.")
      }

      init {
        val chain = middlewares.map { middleware -> middleware(store) }

        dispatcher = chain
            .reversed()
            .fold(store::dispatch as Dispatcher) { dispatch, carryingMiddleware ->
              carryingMiddleware(dispatch)
            }
      }

      override val state: T
        get() = store.state

      override val isDispatching: Boolean
        get() = store.isDispatching

      override fun dispatch(action: Action): Action {

        return dispatcher(action)
      }

      override fun replaceReducer(reducer: Reducer<T>) =
          store.replaceReducer(reducer)

      override fun subscribe(listener: Subscriber<T>): Disposable =
          store.subscribe(listener)

      override fun unsubscribe(disposable: Disposable) =
          store.unsubscribe(disposable)

    }
  }
}
