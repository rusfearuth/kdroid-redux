package su.rusfearuth.arch.kdroidredux.rx.store

import io.reactivex.disposables.Disposable
import su.rusfearuth.arch.kdroidredux.core.interfaces.CommonStore
import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber

interface Store<T: State> : CommonStore<T> {

  fun subscribe(listener: Subscriber<T>): Disposable

  fun unsubscribe(disposable: Disposable)

}