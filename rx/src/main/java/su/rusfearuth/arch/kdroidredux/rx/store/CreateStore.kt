package su.rusfearuth.arch.kdroidredux.rx.store

import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import su.rusfearuth.arch.kdroidredux.core.actions.InitAction
import su.rusfearuth.arch.kdroidredux.core.actions.ReplaceAction
import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.middleware.Enhancer
import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber


fun <T: State> createStore(reducer: Reducer<T>,
                           preloadedState: T,
                           enhancer: Enhancer<Store<T>>? = null): Store<T> {

  val store = object : Store<T> {

    private var isDispatchingNow: Boolean = false
    private var currentReducer = reducer
    private var originalState: T = preloadedState
    private val stateSubject: BehaviorSubject<T> = BehaviorSubject.create()
    private var currentListeners: MutableList<Disposable> = mutableListOf()

    override val state: T
      get() {
        if (isDispatchingNow) {
          throw Throwable("You may not call store.getState() while the reducer is executing. " +
                              "The reducer has already received the state as an argument. " +
                              "Pass it down from the top reducer instead of reading it from the store.")
        }
        @Suppress("UNCHECKED_CAST")
        return originalState.makeCopy() as T
      }


    override val isDispatching: Boolean
      get() = isDispatchingNow


    override fun dispatch(action: Action): Action {
      if (isDispatchingNow) {
        throw Throwable("Reducers may not dispatch actions.")
      }

      // Hash code of state before reducing
      val oldState = state.hashCode()

      try {
        isDispatchingNow = true
        originalState = currentReducer(originalState, action)
      }
      finally {
        isDispatchingNow = false
      }

      // Hash code of updated state
      val newState = state.hashCode()

      // If nothing has changed just go out
      if (action.isNotSystemAction() && newState == oldState) {
        return action
      }

      invalidateListeners()

      stateSubject.onNext(state)

      return action
    }


    override fun replaceReducer(reducer: Reducer<T>) {
      currentReducer = reducer
      dispatch(ReplaceAction)
    }


    override fun subscribe(listener: Subscriber<T>): Disposable {
      if (isDispatching) {
        throw Throwable("You may not call store.subscribe() while the reducer is executing. " +
                            "If you would like to be notified after the store has been updated, subscribe from a " + "component and invoke store.getState() in the callback to access the latest state. " + "See https://redux.js.org/api-reference/store#subscribe(listener) for more details.")
      }
      val disposable = stateSubject.subscribe(listener)
      currentListeners.add(disposable)

      return disposable
    }


    override fun unsubscribe(disposable: Disposable) {
      if (!disposable.isDisposed) {
        disposable.dispose()
      }

      if (!currentListeners.contains(disposable)) {
        return
      }

      currentListeners.remove(disposable)
    }

    private fun Action.isNotSystemAction(): Boolean =
        this !is InitAction && this !is ReplaceAction

    private fun invalidateListeners() {
      currentListeners = currentListeners
          .filter { !it.isDisposed }
          .toMutableList()
    }
  }

  store.dispatch(InitAction)

  if (enhancer != null) {
    return enhancer(store)
  }

  return store
}
