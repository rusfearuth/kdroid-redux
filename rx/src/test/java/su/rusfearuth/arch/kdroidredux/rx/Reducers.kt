package su.rusfearuth.arch.kdroidredux.rx

import su.rusfearuth.arch.kdroidredux.core.actions.InitAction
import su.rusfearuth.arch.kdroidredux.core.actions.ReplaceAction
import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer


const val INIT_MESSAGE = "Message was initialized"
const val REPLACE_MESSAGE = "Reducer was replaced"
const val HELLO_MESSAGE = "Hello was dispatched"


val reducerA: Reducer<PureTestState> = {state, action ->
  when(action) {
    is InitAction -> state.copy(message = INIT_MESSAGE)
    else -> state
  }
}


fun reducerB(state: PureTestState, action: Action): PureTestState =
    when(action) {
      is ReplaceAction -> state.copy(message = REPLACE_MESSAGE)
      else -> state
    }


val reducerC: Reducer<PureTestState> = {state, action ->
  when(action) {
    is HelloActions -> state.copy(message = HELLO_MESSAGE)
    else -> state
  }
}