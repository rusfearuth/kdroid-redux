package su.rusfearuth.arch.kdroidredux.rx.middleware

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import su.rusfearuth.arch.kdroidredux.core.middleware.Middleware
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.rx.*
import su.rusfearuth.arch.kdroidredux.rx.store.Store
import su.rusfearuth.arch.kdroidredux.rx.store.createStore


class `Middlewares` {

  private lateinit var store: Store<PureTestState>


  @Test
  fun `should be called on dispatching`() {
    val reducer = combineReducers(reducerA, ::reducerB, reducerC)
    val initialState = PureTestState("")

    var flagA = Pair(false, false)
    var flagB = Pair(false, false)

    val middlewareA: Middleware<PureTestState> = {
      _ ->
      {
        next ->
        {
          action ->
            flagA = flagA.copy(first = true)
            val nextValue = next(action)
            flagA = flagA.copy(second = true)
            nextValue
        }
      }
    }

    val middlewareB: Middleware<PureTestState> = {
      _ ->
      {
        next ->
        {
          action ->
          flagB = flagB.copy(first = true)
          val nextValue = next(action)
          flagB = flagB.copy(second = true)
          nextValue
        }
      }

    }

    store = createStore(reducer,
                        initialState,
                        applyMiddleware(middlewareA, middlewareB))

    store.dispatch(HelloActions())

    assertEquals("Hello action was dispatched",
                 HELLO_MESSAGE,
                 store.state.message)

    assertTrue("Middleware A flag was updated before calling next",
               flagA.first)
    assertTrue("Middleware A flag was updated after calling next",
               flagA.second)

    assertTrue("Middleware B flag was updated before calling next",
               flagB.first)
    assertTrue("Middleware B flag was updated after calling next",
               flagB.second)
  }
}