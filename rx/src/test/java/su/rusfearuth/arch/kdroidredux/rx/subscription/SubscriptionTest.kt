package su.rusfearuth.arch.kdroidredux.rx.subscription

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.rx.PureTestState
import su.rusfearuth.arch.kdroidredux.rx.reducerA
import su.rusfearuth.arch.kdroidredux.rx.reducerB
import su.rusfearuth.arch.kdroidredux.rx.reducerC
import su.rusfearuth.arch.kdroidredux.rx.store.Store
import su.rusfearuth.arch.kdroidredux.rx.store.createStore

class `Subscriptions` {

  private lateinit var store: Store<PureTestState>

  @Before
  fun `init store`() {
    val reducer = combineReducers(reducerA, ::reducerB,
                                  reducerC)
    val initialState = PureTestState("")

    store = createStore(reducer,
                        initialState)
  }

  @Test
  fun `should unsubscribe themeself`() {
    val subscription = store.subscribe {}

    assertTrue("Subscription is alive", !subscription.isDisposed)

    subscription.dispose()

    assertTrue("Subscription was unsabscribe", subscription.isDisposed)
  }
}