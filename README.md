# KDroid Redux

[![Core Release](https://jitpack.io/v/com.gitlab.rusfearuth.kdroid-redux/core.svg)](https://jitpack.io/#com.gitlab.rusfearuth.kdroid-redux/core) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/079608781b6a40249f83e8ad9018007b)](https://www.codacy.com/app/rusfearuth/kdroid-redux?utm_source=gitlab.com&utm_medium=referral&utm_content=rusfearuth/kdroid-redux&utm_campaign=Badge_Grade)

[KDroidRedux](https://github.com/rusfearuth/kdroid-redux) is implementation [Redux](https://redux.js.org/) for [Android](https://www.android.com/) on [Kotlin](http://kotlinlang.org/).

## Getting started

First of all, you need to add mavent to your main build.gradle file at the root of your project.

Edit _projectDir/build.gradle_:

```
...
allprojects {
  repositories {
    ...
    maven { url 'https://jitpack.io' } // <-- Add repository here
  }
}
...
```

After, you should add core library into your app dependencies list.

Edit _projectDir/app/build.gradle_:

```
dependencies {
  ...
  // if you use gradle 3.0.1 plugin
  implementation 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1'  // <----

  // or if you use previous version of gradle plugin
  compile 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1' // <-----
}
```

Current version of [KDroidRedux](https://github.com/rusfearuth/kdroid-redux) has got 2 implementation of Store `Rx` and `Pure`. It up to you what kind of Store choose.

You should add chosen module to app dependencies list.

Pure version sample _projectDir/app/build.gradle_:

```
dependencies {
  ...
  // if you use gradle 3.0.1 plugin
  implementation 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1'
  implementation 'com.gitlab.rusfearuth.kdroid-redux:pure:2.0.1' // <-----

  // or if you use previous version of gradle plugin
  compile 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1'
  compile 'com.gitlab.rusfearuth.kdroid-redux:pure:2.0.1' // <-----

}
```

Rx version sample _projectDir/app/build.gradle_:

```
dependencies {
  ...
  // if you use gradle 3.0.1 plugin
  implementation 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1'
  implementation 'com.gitlab.rusfearuth.kdroid-redux:rx:2.0.1' // <----
  implementation 'io.reactivex.rxjava2:rxandroid:$rxAndroidVersion' // <-----
  implementation 'io.reactivex.rxjava2:rxjava:$rxJavaVersion' // <-----

  // or if you use previous version of gradle plugin
  compile 'com.gitlab.rusfearuth.kdroid-redux:core:2.0.1'
  compile 'com.gitlab.rusfearuth.kdroid-redux:rx:2.0.1' // <-----
  compile 'io.reactivex.rxjava2:rxandroid:$rxAndroidVersion' // <-----
  compile 'io.reactivex.rxjava2:rxjava:$rxJavaVersion' // <-----
}
```
