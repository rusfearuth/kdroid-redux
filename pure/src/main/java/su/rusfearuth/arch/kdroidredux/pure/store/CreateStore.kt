package su.rusfearuth.arch.kdroidredux.pure.store

import su.rusfearuth.arch.kdroidredux.core.actions.InitAction
import su.rusfearuth.arch.kdroidredux.core.actions.ReplaceAction
import su.rusfearuth.arch.kdroidredux.core.interfaces.Action
import su.rusfearuth.arch.kdroidredux.core.middleware.Enhancer
import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer
import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber
import su.rusfearuth.arch.kdroidredux.pure.subscription.Subscription


fun <T: State> createStore(reducer: Reducer<T>,
                           preloadedState: T,
                           enhancer: Enhancer<Store<T>>? = null): Store<T> {

  val store = object : Store<T> {

    private var isDispatchingNow: Boolean = false
    private var currentReducer = reducer
    private var originalState: T = preloadedState
    private var currentListeners: List<Subscription<T>> = emptyList()
    private var nextListeners: MutableList<Subscription<T>> = mutableListOf()

    override val state: T
      get() {
        if (isDispatchingNow) {
          throw Throwable("You may not call store.getState() while the reducer is executing. " +
                              "The reducer has already received the state as an argument. " +
                              "Pass it down from the top reducer instead of reading it from the store.")
        }
        // TODO: make copy
        @Suppress("UNCHECKED_CAST")
        return originalState.makeCopy() as T
      }


    override val isDispatching: Boolean
      get() = isDispatchingNow


    override fun dispatch(action: Action): Action {
      if (isDispatchingNow) {
        throw Throwable("Reducers may not dispatch actions.")
      }

      // Hash code of state before reducing
      val oldState = state.hashCode()

      try {
        isDispatchingNow = true
        originalState = currentReducer(originalState, action)
      }
      finally {
        isDispatchingNow = false
      }

      // Hash code of updated state
      val newState = state.hashCode()

      // If nothing has changed just go out
      if (action.isNotSystemAction() && newState == oldState) {
        return action
      }

      currentListeners = nextListeners.toList()

      currentListeners.forEach { it.updated(state) }

      return action
    }


    override fun replaceReducer(reducer: Reducer<T>) {
      currentReducer = reducer
      dispatch(ReplaceAction)
    }


    override fun subscribe(listener: Subscriber<T>): Subscription<T> {
      if (isDispatching) {
        throw Throwable("You may not call store.subscribe() while the reducer is executing. " +
                            "If you would like to be notified after the store has been updated, subscribe from a " +
                            "component and invoke store.getState() in the callback to access the latest state. " +
                            "See https://redux.js.org/api-reference/store#subscribe(listener) for more details.")
      }
      val subscription = Subscription(this, listener)
      nextListeners.add(subscription)
      listener(state)
      return subscription
    }


    override fun unsubscribe(subscription: Subscription<T>) {
      if (!nextListeners.contains(subscription)) {
        return
      }
      subscription.unsubscribe(isItSelf = false)
      nextListeners.remove(subscription)
    }

    private fun Action.isNotSystemAction(): Boolean =
        this !is InitAction && this !is ReplaceAction
  }

  store.dispatch(InitAction)

  if (enhancer != null) {
    return enhancer(store)
  }

  return store
}
