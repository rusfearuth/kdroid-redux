package su.rusfearuth.arch.kdroidredux.pure.subscription

import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber
import su.rusfearuth.arch.kdroidredux.pure.store.Store


class Subscription<T: State>(private val store: Store<T>,
                             private val listener: Subscriber<T>) {

  private var isSubscribedInternal = true

  val isSubscribed: Boolean
    get() = isSubscribedInternal

  fun updated(state: T) =
      listener(state)

  fun unsubscribe(isItSelf: Boolean = true) {
    if (!isSubscribedInternal) {
      return
    }

    if (!isItSelf) {
      isSubscribedInternal = false
      return
    }

    if (store.isDispatching) {
      throw Throwable("You may not unsubscribe from a store listener while the reducer is executing. " +
                          "See https://redux.js.org/api-reference/store#subscribe(listener) for more details.")
    }

    store.unsubscribe(this)
    isSubscribedInternal = false
  }
}
