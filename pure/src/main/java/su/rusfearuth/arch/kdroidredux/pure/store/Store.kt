package su.rusfearuth.arch.kdroidredux.pure.store

import su.rusfearuth.arch.kdroidredux.core.interfaces.CommonStore
import su.rusfearuth.arch.kdroidredux.core.interfaces.State
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber
import su.rusfearuth.arch.kdroidredux.pure.subscription.Subscription


interface Store<T: State> : CommonStore<T> {

  fun subscribe(listener: Subscriber<T>): Subscription<T>

  fun unsubscribe(subscription: Subscription<T>)

}