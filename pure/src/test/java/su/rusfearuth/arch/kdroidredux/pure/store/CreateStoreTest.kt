package su.rusfearuth.arch.kdroidredux.pure.store

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.pure.*
import su.rusfearuth.arch.kdroidredux.core.subscription.Subscriber

class `Stores` {

  private lateinit var store: Store<PureTestState>

  @Before
  fun `init store`() {
    val reducer = combineReducers(reducerA, ::reducerB,
                                  reducerC)
    val initialState = PureTestState("")

    store = createStore(reducer,
                                                                  initialState)
  }

  @Test
  fun `should be create by means createStore`() {
    assertNotNull("Store was created", store)
    assertEquals("Store state after initialization is $INIT_MESSAGE",
                 INIT_MESSAGE,
                 store.state.message)
  }

  @Test
  fun `should allow to replace reducer`() {
    store.replaceReducer(::reducerB)

    assertEquals("Store state after replacing reducer is $REPLACE_MESSAGE",
                 REPLACE_MESSAGE,
                 store.state.message)
  }

  @Test
  fun `should dispatch custom action, such as HelloAction`() {
    store.dispatch(HelloActions())

    assertEquals("Store dispatched HelloAction",
                 HELLO_MESSAGE,
                 store.state.message)
  }

  @Test
  fun `should allow to subscribe on updates`() {
    var flag = false
    val subscriber: Subscriber<PureTestState> = { flag = true }

    store.subscribe(subscriber)

    store.dispatch(HelloActions())

    assertTrue("Subscription should be called", flag)
  }

  @Test
  fun `should allow to unsubscribe from updates`() {
    val subscriber: Subscriber<PureTestState> = {}

    val subscription = store.subscribe(subscriber)

    assertTrue("Subscription is active", subscription.isSubscribed)

    store.unsubscribe(subscription)

    assertFalse("Subscription is not active", subscription.isSubscribed)
  }
}