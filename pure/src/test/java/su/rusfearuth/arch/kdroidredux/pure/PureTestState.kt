package su.rusfearuth.arch.kdroidredux.pure

import su.rusfearuth.arch.kdroidredux.core.interfaces.State

data class PureTestState(val message: String) : State {
  override fun makeCopy(): PureTestState = copy()
}