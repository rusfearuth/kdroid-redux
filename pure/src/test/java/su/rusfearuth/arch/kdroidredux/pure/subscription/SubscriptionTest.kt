package su.rusfearuth.arch.kdroidredux.pure.subscription

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.pure.PureTestState
import su.rusfearuth.arch.kdroidredux.pure.reducerA
import su.rusfearuth.arch.kdroidredux.pure.reducerB
import su.rusfearuth.arch.kdroidredux.pure.reducerC
import su.rusfearuth.arch.kdroidredux.pure.store.Store
import su.rusfearuth.arch.kdroidredux.pure.store.createStore

class `Subscriptions` {

  private lateinit var store: Store<PureTestState>

  @Before
  fun `init store`() {
    val reducer = combineReducers(reducerA, ::reducerB,
                                  reducerC)
    val initialState = PureTestState("")

    store = createStore(reducer,
                        initialState)
  }

  @Test
  fun `should unsubscribe themeself`() {
    val subscription = store.subscribe {}

    assertTrue("Subscription is alive", subscription.isSubscribed)

    subscription.unsubscribe()

    assertFalse("Subscription was unsabscribe", subscription.isSubscribed)
  }
}