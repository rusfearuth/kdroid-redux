package su.rusfearuth.arch.kdroidredux.sample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.pure.middleware.applyMiddleware
import su.rusfearuth.arch.kdroidredux.pure.store.createStore
import su.rusfearuth.arch.kdroidredux.pure.subscription.Subscription


class MainActivity : AppCompatActivity() {

  private val store = createStore(combineReducers(counterReducer),
                                  CounterState(),
                                  applyMiddleware(logger))
  private var state = store.state
  private lateinit var subscription: Subscription<CounterState>

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    btnIncrease.setOnClickListener {
      store.dispatch(IncreaseAction)
    }

    btnDecrease.setOnClickListener {
      store.dispatch(DecreaseAction)
    }

    btnSayHello.setOnClickListener {
      store.dispatch(SayHiAction("Hi there!"))
    }

    subscription = store.subscribe {

      renderState(it)

      state = it
    }
  }

  override fun onDestroy() {
    subscription.unsubscribe()
    super.onDestroy()
  }

  private fun renderState(state: CounterState) {
    message.text = state.message
    counter.text = state.count.toString()
  }
}
