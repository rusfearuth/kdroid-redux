package su.rusfearuth.arch.kdroidredux.sample

import su.rusfearuth.arch.kdroidredux.core.reducer.Reducer


val counterReducer: Reducer<CounterState> = { state, account ->
  when (account) {
    is IncreaseAction -> state.copy(count = state.count + 1)
    is DecreaseAction -> state.copy(count = state.count - 1)
    is SayHiAction -> state.copy(message = account.message)
    else -> state
  }
}