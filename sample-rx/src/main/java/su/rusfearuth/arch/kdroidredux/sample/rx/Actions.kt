package su.rusfearuth.arch.kdroidredux.sample.rx

import su.rusfearuth.arch.kdroidredux.core.interfaces.Action


object IncreaseAction : Action


object DecreaseAction : Action


data class SayHiAction(val message: String) : Action