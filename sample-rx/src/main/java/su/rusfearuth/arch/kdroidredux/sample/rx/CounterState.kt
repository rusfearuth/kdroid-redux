package su.rusfearuth.arch.kdroidredux.sample.rx

import su.rusfearuth.arch.kdroidredux.core.interfaces.State


data class CounterState(
    val count: Long = 0L,
    val message: String = ""
) : State {
  override fun makeCopy(): CounterState = copy()
}