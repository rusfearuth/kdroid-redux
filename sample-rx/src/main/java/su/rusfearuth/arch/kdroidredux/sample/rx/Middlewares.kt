package su.rusfearuth.arch.kdroidredux.sample.rx

import android.util.Log
import su.rusfearuth.arch.kdroidredux.core.middleware.Middleware


val logger: Middleware<CounterState> = {
  store ->
  {
    next ->
    {
      action ->
      Log.v("LOGGER", "Got action ${action}")
      val nextValue = next(action)
      Log.v("LOGGER", "After dispatcher called")
      nextValue
    }
  }
}