package su.rusfearuth.arch.kdroidredux.sample.rx

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import su.rusfearuth.arch.kdroidredux.core.reducer.combineReducers
import su.rusfearuth.arch.kdroidredux.rx.middleware.applyMiddleware
import su.rusfearuth.arch.kdroidredux.rx.store.createStore
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

  private val store = createStore(combineReducers(counterReducer),
                                  CounterState(),
                                  applyMiddleware(logger))
  private var state = store.state
  private lateinit var disposible: Disposable

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    btnIncrease.setOnClickListener {
      store.dispatch(IncreaseAction)
    }

    btnDecrease.setOnClickListener {
      store.dispatch(DecreaseAction)
    }

    btnSayHello.setOnClickListener {
      store.dispatch(SayHiAction("Hi there!"))
    }

    disposible = store.subscribe {

      renderState(it)

      state = it
    }
  }

  override fun onDestroy() {
    disposible.dispose()
    super.onDestroy()
  }

  private fun renderState(state: CounterState) {
    message.text = state.message
    counter.text = state.count.toString()
  }
}
